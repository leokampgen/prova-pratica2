using NUnit.Framework;
using ProjetoGit;


namespace ProjetoGit.Tests
{
    public class Tests
    {

        [Test]
        public void CPF()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("04494263001"));
        }

        [Test]
        public void CNPJ()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("57173934000142"));
        }
    }

}